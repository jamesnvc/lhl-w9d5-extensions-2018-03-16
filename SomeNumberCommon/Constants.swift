//
//  Constants.swift
//  SomeNumberSharing
//
//  Created by James Cash on 16-03-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import Foundation

public let KEY_NAME = "com.occasionallycogent.SavedNumber"
public let GROUP_NAME = "group.occasionallycogent.testing"
