//
//  SomeNumberCommon.h
//  SomeNumberCommon
//
//  Created by James Cash on 16-03-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for SomeNumberCommon.
FOUNDATION_EXPORT double SomeNumberCommonVersionNumber;

//! Project version string for SomeNumberCommon.
FOUNDATION_EXPORT const unsigned char SomeNumberCommonVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <SomeNumberCommon/PublicHeader.h>
