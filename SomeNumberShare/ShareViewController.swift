//
//  ShareViewController.swift
//  SomeNumberShare
//
//  Created by James Cash on 16-03-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit
import Social
import SomeNumberCommon

class ShareViewController: SLComposeServiceViewController {

    override func isContentValid() -> Bool {
        let numberFormatter = NumberFormatter()
        numberFormatter.allowsFloats = true
        numberFormatter.maximum = NSNumber(floatLiteral: 100)
        numberFormatter.minimum = NSNumber(floatLiteral: 0)
        return (numberFormatter.number(from: contentText) != nil)
        // Do validation of contentText and/or NSExtensionContext attachments here
    }

    override func didSelectPost() {
        // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
        let numberFormatter = NumberFormatter()
        numberFormatter.allowsFloats = true
        numberFormatter.maximum = NSNumber(floatLiteral: 100)
        numberFormatter.minimum = NSNumber(floatLiteral: 0)
        let number: Float = numberFormatter.number(from: contentText)!.floatValue
        UserDefaults(suiteName: GROUP_NAME)!.set(number, forKey: KEY_NAME)
    
        // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
        self.extensionContext!.completeRequest(returningItems: [], completionHandler: nil)
    }

    override func configurationItems() -> [Any]! {
        // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
        return []
    }

}
