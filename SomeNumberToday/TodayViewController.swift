//
//  TodayViewController.swift
//  SomeNumberToday
//nu
//  Created by James Cash on 16-03-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit
import NotificationCenter
import SomeNumberCommon

class TodayViewController: UIViewController, NCWidgetProviding {
        
    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var numberStepper: UIStepper!
    var number: Float = 0

    override func viewDidLoad() {
        super.viewDidLoad()
        number = UserDefaults(suiteName: GROUP_NAME)!.float(forKey: KEY_NAME)
        numberLabel.text = "\(number)"
        numberStepper.value = Double(number)
        // Do any additional setup after loading the view from its nib.
    }
    
    @IBAction func changeNumber(_ sender: UIStepper) {
        number = Float(sender.value)
        UserDefaults(suiteName: GROUP_NAME)!.set(number, forKey: KEY_NAME)
        numberLabel.text = "\(number)"
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        completionHandler(NCUpdateResult.newData)
    }
    
}
