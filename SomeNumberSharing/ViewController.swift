//
//  ViewController.swift
//  SomeNumberSharing
//
//  Created by James Cash on 16-03-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit
import SomeNumberCommon

class ViewController: UIViewController {

    @IBOutlet var numberLabel: UILabel!
    @IBOutlet var numberSlider: UISlider!

    var number: Float = 0

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        restoreSavedValue()
    }

    @IBAction func numberChanged(_ sender: UISlider) {
        number = sender.value
        UserDefaults(suiteName: GROUP_NAME)!.set(number, forKey: KEY_NAME)
        numberLabel.text = "\(number)"
    }

    func restoreSavedValue() {
        number = UserDefaults(suiteName: GROUP_NAME)!.float(forKey: KEY_NAME)
        numberLabel.text = "\(number)"
        numberSlider.value = number
    }
    
}
